#pragma once

class FVec{
public:
	//section of RuleOf5 declarations
    FVec(int sizeOfVector);
   
	//section of overload non-member operators
	FVec operator+(int & additionalValue) const; // think about using readable variable names 
	FVec operator+(const FVec & fvec) const;

	//section of type functions
    FVec set(int pos, int val) const;

    void print(const char* suffixMessage) const; // take Your convention of defining methos in header in the same tamplate (use variable name, also when is not mandatory).
	// In future it will help You to generate documentation from header files.



private:
	int _fVectorSize; // if You wonna have more "length arguments in mmebers of specific type there will be mismatch
	int * _fVectorData_ptr; // same as above (not always) tab != vector
};