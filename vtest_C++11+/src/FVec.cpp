#include <iostream>
#include "FVec.h"

using namespace std; // use aliases in implementation file provide better readability for other programmers. 
// reason: count of use "std::" in more complex code of implementation is to high to "run-time-read"

FVec::FVec(int sizeOfVector): _fVectorSize(sizeOfVector) { //what mean "a"? Whole code indetifiers should be logic-named for every programmer.
	// Instead "a" use e.g. "sizeOfVector". Then also lexical heleprs whos this name while programming in some of IDE's.
        _fVectorData_ptr = new int [_fVectorSize];
        for(int i=0; i < _fVectorSize; i++){
            _fVectorData_ptr[i]=0;
        } 
    }


FVec FVec::set(int pos, int val) const{
    this->_fVectorData_ptr[pos] = val;
    return *this;
}

void FVec::print(const char* suffixMessage) const{ // what mean "a" ? [...] Maybe suffixMessage wolud be more readable?
    cout << suffixMessage;
    for(int i=0; i<_fVectorSize; i++){
		cout << " " << _fVectorData_ptr[i]; // always use correct indetaion, it provide readability for more complex algorithms and 
		// is usable for lexical purposes e.g. Python.
    }
    cout << endl; // prefer to use endl than \n char to provide ostream flush!
}

FVec FVec::operator+ (int& additionalValue) const{ //pass arg by reference, no need of copy constructor/ cpu work
    for(int i=0; i < _fVectorSize; i++){
        this->_fVectorData_ptr[i] += additionalValue; // error: works only for val=10, now works for every val in range of int-type
		// when using "return "this"" also use explicity this pointer for this members in a function ( provide readability)
    }
    return *this;
}
    

FVec FVec::operator+ (const FVec & fvec) const{ //pass arg by reference, no need of copy constructor/ cpu work
    for (int i=0; i<_fVectorSize; i++){
		fvec._fVectorData_ptr[i] += 10;
    }
    return *this;
}
    